# [fsharp](https://aur.archlinux.org/packages/fsharp/)

The F# compiler, FSharp.Core library, and tools for F# https://www.microsoft.com/net/learn/languages/fsharp

# Official documentation
* [fsharp.org](https://fsharp.org/)
* [*Computation Expressions*
  ](https://learn.microsoft.com/en-us/dotnet/fsharp/language-reference/computation-expressions)
  2023-07
  * Equivalent of Haskell Do-Notation
* [*Some Details on F# Computation Expressions*
  ](https://learn.microsoft.com/fr-fr/archive/blogs/dsyme/some-details-on-f-computation-expressions)
  2007-09

## Download and install (or not)
* [*Get Started with F#*
  ](https://docs.microsoft.com/en-US/dotnet/fsharp/get-started/)
  Microsoft 2020-09
* [*Use F# on Linux*](https://fsharp.org/use/linux/)
* [fsharp](https://repology.org/project/fsharp/versions) (repology)

## Tutorial
* [*F# Tutorial - Hello World in 10 minutes*
  ](https://dotnet.microsoft.com/learn/languages/fsharp-hello-world-tutorial/intro)

# Unofficial documentation
* [*F_Sharp_(programming_language)*
  ](https://en.m.wikipedia.org/wiki/F_Sharp_(programming_language))
---
* [*Making Http Requests in F#*
  ](https://dev.to/tunaxor/making-http-requests-in-f-1n0b)
  2021-03 2022-05 Angel Daniel Munoz Gonzalez
* [*Dark's new backend will be in F#*
  ](https://blog.darklang.com/new-backend-fsharp/)
  2020-11 Paul Biggar
* [*F# Computation Expressions, 'do' notation and list comprehensions*
  ](https://github.com/dsyme/dsyme-presentations/blob/master/design-notes/ces-compared.md)
  2020-02 Phillip Wadler and Don Syme

## Reader Monad
* [F# Reader Monad
  ](https://www.google.com/search?q=F%23+Reader+Monad)

## F\# for Fun and Profit
* Six approaches to dependency injection

  3.  [*Dependency injection using the Reader monad*
      ](https://fsharpforfunandprofit.com/posts/dependencies-3/)
      2020-12
* [*Designing with types: Making illegal states unrepresentable*
  ](https://fsharpforfunandprofit.com/posts/designing-with-types-making-illegal-states-unrepresentable/)
  2013-01
* [The "Computation Expressions" series
  ](https://fsharpforfunandprofit.com/series/computation-expressions/)
  (2013-01)
  * Equivalent of Haskell Do-Notation

## Books
* [ti:f sharp](https://worldcat.org/search?q=ti:f+sharp)
* [ti:fsharp](https://worldcat.org/search?q=ti:fsharp)
---
* *F# in Action*
  2024-05 Isaac Abraham (Manning Publications)
* *Stylish F# 6: Crafting Elegant Functional Code for .NET 6*
  2021-12 (Second Edition) Kit Eason (Apress)
  * 2018-11 Stylish F#: Crafting Elegant Functional Code for .NET and .NET Core
* *Get programming with F# : a guide for .NET developers*
  2018-03 Isaac Abraham
* [*Domain modeling made functional : tackle software complexity with domain-driven design and F♯*
  ](https://worldcat.org/search?q=ti%3ADomain+modeling+made+functional+%3A+tackle+software+complexity+with+domain-driven+design+and+F%E2%99%AF)
  2018
* *Concurrency in .NET : modern patterns of concurrent and parallel programming*
  2018
* *Learning Functional Programming with F#*
  2017-08 Onur Gumus
* *F♯ deep dives*
  2015 Tomas Petricek
* *The book of F♯ : breaking free with managed functional programming*
  2014 Dave Fancher
* *Functional programming using F#*
  2013 Michael R. Hansen
* [*Building web, cloud, and mobile solutions with F♯*
  ](http://worldcat.org/oclc/830052059)
  2013
* *Programming F♯ 3.0*
  2012 (Second edition) Chris Smith
* *Real-world functional programming : with examples in F# and C#*
  2010 Tomas Petricek
* [*Programming F#*
  ](https://www.oreilly.com/library/view/programming-f/9780596802080/)
  2009-10 Chris Smith (O'Reilly Media, Inc.)
* *F# for scientists*
  2008 Jon D. Harrop
* [*2007 Foundations of F♯*](http://worldcat.org/oclc/255475142)

# Implementations
* [*Using F# for Web Applications*
  ](https://fsharp.org/use/web-apps)
  (fsharp.org)

## Fable (F# to JavaScript, TypeScript, Python, Rust and Dart Compiler)
* http://fable.io
* https://github.com/fable-compiler/fable

# Comparing F# with other languages
* [*Elixir vs F# – opinionated syntax comparison*
  ](https://mlusiak.com/elixir-vs-fsharp/)
  2017-03 Michał Łusiak
