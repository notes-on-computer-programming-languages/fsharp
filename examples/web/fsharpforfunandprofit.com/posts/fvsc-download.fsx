// "open" brings a .NET namespace into visibility
open System.Net
open System
open System.IO

// Maybe rewritten using:

// Making Http Requests in F#
// https://dev.to/tunaxor/making-http-requests-in-f-1n0b
// 2021-2022 Angel Daniel Munoz Gonzalez

// Fetch the contents of a web page
let fetchUrl callback url =
    let req = WebRequest.Create(Uri(url))
    use resp = req.GetResponse()
    use stream = resp.GetResponseStream()
    use reader = new IO.StreamReader(stream)
    callback reader url

let myCallback (reader:IO.StreamReader) url =
    let html = reader.ReadToEnd()
    let html100 = html.Substring(0,100)
    printfn "Downloaded %s. First 100 is %s" url html100
    html      // return all the html

//test
let google_1 = fetchUrl myCallback "http://google.com"

// build a function with the callback "baked in"
let fetchUrl2 = fetchUrl myCallback

// test
let google_2 = fetchUrl2 "http://www.google.com"
let bbc      = fetchUrl2 "http://news.bbc.co.uk"

// test with a list of sites
let sites = ["http://www.bing.com";
             "http://www.google.com"]
             // "http://www.yahoo.com"]

// process each site in the list
sites |> List.map fetchUrl2
