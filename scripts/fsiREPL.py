#! /usr/bin/python3

from __future__ import unicode_literals

import pexpect
import pexpect.replwrap
import sys

c = pexpect.replwrap.REPLWrapper(
        pexpect.spawnu(
            command='dotnet fsi --readline- --consolecolors+',
            echo=False,
            logfile=sys.stdout,
            timeout=5,
            encoding='utf-8'
        ),
        orig_prompt='\r\n> ',
        continuation_prompt='\r\n- ',
        prompt_change=None
    )
with open(sys.argv[1]) as fp:
    for line in fp:
        # print('#')
        # print(line)
        # print(line.strip())
        # print('+'+c.run_command(line.strip()))
        c.run_command(line.rstrip())
        # c.run_command(line)
